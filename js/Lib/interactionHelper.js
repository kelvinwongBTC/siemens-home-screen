/*
 * helper object for interaction
 * */
var interactionHelper = new function () {
}

/*
 * add Touch event helper
 * */
interactionHelper.addTouchEvent = function (jQObj, callback) {

    // touch item hightlight
    jQObj.on('touchstart',function (event) {

        $(this).addClass('highightedItem');
        $(this).data('startTime', new Date());

        var touchPageX = event.originalEvent.touches[0].pageX;
        var touchPageY = event.originalEvent.touches[0].pageY;
        var lastTouchElement = document.elementFromPoint(touchPageX, touchPageY);

        $(this).data('targetElement', lastTouchElement);

    }).on('touchend',function (event) {

            $(this).removeClass('highightedItem');

            if ($(window).width() < 768) return;

            var startTime = $(this).data('startTime');
            var touchPageX = event.originalEvent.changedTouches[0].pageX;
            var touchPageY = event.originalEvent.changedTouches[0].pageY;
            var lastTouchElement = document.elementFromPoint(touchPageX, touchPageY);

            var targetElement =  $(this).data('targetElement');

            if (startTime !== null ) {
                if(new Date() - startTime > 400 && lastTouchElement == targetElement) {
                    event.stopPropagation();
                    event.preventDefault();

                    if (callback !== null)
                        callback(event);
                }
            }

            startTime = null;

        });

    interactionHelper.addClickEvent(jQObj, callback);
}

interactionHelper.addClickEvent = function (jQObj, callback) {


    // touch item hightlight
    jQObj.on('click', function (event) {

            event.stopPropagation();
            event.preventDefault();

            if (callback !== null)
                callback(event);
        });
}


