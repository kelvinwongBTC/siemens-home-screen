// jQuery
(function($){

    // on loading
    $(function() {

        var widgetJSONCollection = [];

        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('logo', 'div.widgetRow:nth-child(1) > div:nth-child(1)'));
        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('welcome', 'div.widgetRow:nth-child(1) > div:nth-child(2)'));
        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('search', 'div.widgetRow:nth-child(1) > div:nth-child(3)'));

        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('newStories', 'div.widgetRow:nth-child(2) > div:nth-child(1)'));
        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('topStories', 'div.widgetRow:nth-child(2) > div:nth-child(2)'));
        widgetJSONCollection.push(widgetManager.jsonObjectWithWidgetNameAndjQuerySelectorKey('channels', 'div.widgetRow:nth-child(3) > div:nth-child(1)'));

        widgetManager.setUpAppearance(widgetJSONCollection);
    });

    $('').btcjsapi({
        btcDebugger : function (data) {
            var json = BTCJSAPIHtmlManager.parseJsonData(data);

            if (json) {
                if(json.error) {
                    alert('error occurred : ' + JSON.stringify(json.error));
                }
            }
        }
    });

})(jQuery);